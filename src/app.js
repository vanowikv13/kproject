const express = require('express');
const path = require('path');
const app = express();
const url_ = require('url');
let bodyParser = require('body-parser');
const {
    MongoClient
} = require('mongodb');
const {
    query
} = require('express');
let mongoClient = require('mongodb').MongoClient;
let ObjectId = require('mongodb').ObjectID;
let url = "mongodb://localhost:27017/kpdb";

app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, '/')));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: false
}));
// parse application/json
app.use(bodyParser.json());

//send schema to db
mongoClient.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, function(err, db) {
    let dbo = db.db('kpdb');
    dbo.collection('kpdb').createIndex({ title: "text", description: "text" });
});


app.get('/', function (req, res) {
    mongoClient.connect(url, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }, function (err, db) {
        let dbo = db.db('kpdb');
        let list = [];
        let actual = null;
        let _id = req.query._id;
        let status = req.query.status;

        let cursor = dbo.collection('kpdb').find();

        cursor.each(function (err, item) {
            if (item == null) {
                list.reverse();
                db.close();

                if (list.length > 0 && actual == null && status == undefined)
                    actual = list[0];
                if (actual == null || status == 'newNote')
                    actual = {
                        title: "",
                        description: ""
                    };
                res.render('index.ejs', {
                    list: list,
                    actual: actual,
                });
            } else {
                item._id = item._id.toString();
                if (item._id == _id && _id != undefined)
                    actual = item;
                list.push(item);
            }
        });
    });
});

app.get('/newNote', function (req, res) {
    res.redirect('/?status=newNote');
});

app.get('/select/:_id', function (req, res) {
    let _id = req.params._id;
    res.redirect('/?_id=' + _id);
});

app.get('/search', function (req, res) {
    mongoClient.connect(url, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }, function (err, db) {
        if (err) throw err;
        let dbo = db.db('kpdb');

        dbo.collection('kpdb').find({
            '$text': {
                '$search': req.query.searchText
            }
        }).toArray(function (err, items) {
            items.reverse();
            res.render('index.ejs', {
                list: items,
                actual: {
                    title: "",
                    description: ""
                },
            });
            db.close();
        });
    });

});

app.post('/delete', function (req, res) {
    let _id = req.body._id;
    mongoClient.connect(url, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }, function (err, db) {
        let dbo = db.db('kpdb');

        dbo.collection('kpdb').deleteOne({
            _id: ObjectId(_id)
        }, function (err, result) {
            if (err) throw err;
            res.redirect('/');
        });
    });

});

app.post('/sendNote', function (req, res) {
    mongoClient.connect(url, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }, function (err, db) {
        let dbo = db.db('kpdb');
        let obj = {
            title: req.body.title,
            description: req.body.description
        };

        let filter = {};
        if (req.body._id.length > 1) {
            filter = {
                _id: ObjectId(req.body._id)
            };
            dbo.collection('kpdb').updateOne(filter, {
                $set: obj
            }, {
                upsert: true
            }, function (err, result) {
                if (err) throw err;
                res.redirect('/');
            });
        } else {
            dbo.collection('kpdb').insertOne(obj, function (err, result) {
                if (err) throw err;
                res.redirect('/');
            });
        }
    });
});

app.listen(8000, function () {});